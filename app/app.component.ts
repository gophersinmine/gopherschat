import { Component } from '@angular/core';

import { InputForm } from './input-form/input-form.component';
import { MessageForm} from './message-form/message-form.component';

import { MessageService } from './shared/message.service';


@Component({
    selector: 'chat-app',
    templateUrl : `app/app.component.html`,
    directives:  [InputForm,MessageForm],
    providers: [MessageService] 
})

export class AppComponent{
    
    constructor(){}
}