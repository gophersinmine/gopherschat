import { Component} from '@angular/core';

import { Message } from '../shared/message.modal';
import { MessageService } from '../shared/message.service';
import { HttpService } from '../shared/http.service';
import { SEMANTIC_COMPONENTS,SEMANTIC_DIRECTIVES } from 'ng-semantic';

@Component({
    selector: 'input-form',
    templateUrl : `app/input-form/input-form.component.html`, 
    styleUrls: ['app/input-form/input-form.component.css'],
    directives: [SEMANTIC_COMPONENTS,SEMANTIC_DIRECTIVES],
    providers:  [HttpService]
})

export class InputForm{
    Author: string;
    Message: boolean;
    constructor(private _messageService:MessageService,private _httpService:HttpService){
        this._messageService.connection();        
        this._messageService.parseMessages();
    }

    sendMessage(text:string){
        if (text){
            let message = new Message(this.Author,text);
            this._messageService.sendMessage(message);         
        }
    }

    auth(name:string, password:string,url:string) { 
        this._httpService.postReq(name,password,url).map(res => this.changeAuthor(res['_body'])).subscribe(
                name => name,
                password => password,
                () => url
            );      
    }

    changeAuthor(body:string){
        if (body){
            this.Author = body;
            this.Message = false;
        } else {
            this.Message = true;
        }
        console.log(this.Message);
        
    }
}