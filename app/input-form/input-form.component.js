"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var message_modal_1 = require('../shared/message.modal');
var message_service_1 = require('../shared/message.service');
var http_service_1 = require('../shared/http.service');
var ng_semantic_1 = require('ng-semantic');
var InputForm = (function () {
    function InputForm(_messageService, _httpService) {
        this._messageService = _messageService;
        this._httpService = _httpService;
        this._messageService.connection();
        this._messageService.parseMessages();
    }
    InputForm.prototype.sendMessage = function (text) {
        if (text) {
            var message = new message_modal_1.Message(this.Author, text);
            this._messageService.sendMessage(message);
        }
    };
    InputForm.prototype.auth = function (name, password, url) {
        var _this = this;
        this._httpService.postReq(name, password, url).map(function (res) { return _this.changeAuthor(res['_body']); }).subscribe(function (name) { return name; }, function (password) { return password; }, function () { return url; });
    };
    InputForm.prototype.changeAuthor = function (body) {
        if (body) {
            this.Author = body;
            this.Message = false;
        }
        else {
            this.Message = true;
        }
        console.log(this.Message);
    };
    InputForm = __decorate([
        core_1.Component({
            selector: 'input-form',
            templateUrl: "app/input-form/input-form.component.html",
            styleUrls: ['app/input-form/input-form.component.css'],
            directives: [ng_semantic_1.SEMANTIC_COMPONENTS, ng_semantic_1.SEMANTIC_DIRECTIVES],
            providers: [http_service_1.HttpService]
        }), 
        __metadata('design:paramtypes', [message_service_1.MessageService, http_service_1.HttpService])
    ], InputForm);
    return InputForm;
}());
exports.InputForm = InputForm;
//# sourceMappingURL=input-form.component.js.map