import { HTTP_PROVIDERS } from '@angular/http';
import { bootstrap } from '@angular/platform-browser-dynamic';

import { AppComponent } from './app.component';
import { HttpService } from './shared/http.service';

bootstrap(AppComponent,[HTTP_PROVIDERS]);