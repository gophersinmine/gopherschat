import { Component,Injectable } from '@angular/core';
import { Http,HTTP_PROVIDERS,Headers,RequestOptions,Response } from '@angular/http';

import {Observable}     from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Component({
    providers: [HTTP_PROVIDERS] 
})

@Injectable()
export class HttpService {

    constructor(private _http:Http){}

    postReq(name:string,password:string,url:string){


        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers, method: "post" });
        let user = JSON.stringify({"username":name,"password":password});
        let servUrl = "http://localhost:3000/"
    
        servUrl += url;
        
        return this._http.post(servUrl,user,options)
    }
}