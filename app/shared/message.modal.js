"use strict";
var Message = (function () {
    function Message(author, text) {
        this.author = author;
        this.text = text;
    }
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=message.modal.js.map