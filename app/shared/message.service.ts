import { Injectable } from '@angular/core';

import { $WebSocket } from 'angular2-websocket/angular2-websocket';
import { IMessage } from './message.modal';
import { messages } from './message.date';

@Injectable() 

export class MessageService{

    answear : IMessage;
    ws = new $WebSocket('ws://localhost:3000/chat'); 
     
    connection(){
        this.ws.connect()
    }
    parseMessages(){
      this.ws.onMessage(function (e){     
            this.answear = JSON.parse(e.data);
            messages.push(this.answear);
        },null);
    } 

    sendMessage(message:IMessage){
       this.ws.send(message);
    }

}