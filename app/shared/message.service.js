"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_websocket_1 = require('angular2-websocket/angular2-websocket');
var message_date_1 = require('./message.date');
var MessageService = (function () {
    function MessageService() {
        this.ws = new angular2_websocket_1.$WebSocket('ws://localhost:3000/chat');
    }
    MessageService.prototype.connection = function () {
        this.ws.connect();
    };
    MessageService.prototype.parseMessages = function () {
        this.ws.onMessage(function (e) {
            this.answear = JSON.parse(e.data);
            message_date_1.messages.push(this.answear);
        }, null);
    };
    MessageService.prototype.sendMessage = function (message) {
        this.ws.send(message);
    };
    MessageService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], MessageService);
    return MessageService;
}());
exports.MessageService = MessageService;
//# sourceMappingURL=message.service.js.map