export interface IMessage{
    author : string;
    text   : string;
}

export class Message implements IMessage{
    author : string;
    text   : string;

    constructor(author:string,text:string){
        this.author = author;
        this.text   = text;
    }
}