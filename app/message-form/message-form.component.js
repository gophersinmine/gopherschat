"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var message_date_1 = require('../shared/message.date');
var MessageForm = (function () {
    function MessageForm() {
        this.messages = message_date_1.messages;
    }
    MessageForm.prototype.ngOnInit = function () {
        this.scrollToBottom();
    };
    MessageForm.prototype.ngAfterViewChecked = function () {
        this.scrollToBottom();
    };
    MessageForm.prototype.scrollToBottom = function () {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
        catch (err) { }
    };
    __decorate([
        core_1.ViewChild('msgs'), 
        __metadata('design:type', core_1.ElementRef)
    ], MessageForm.prototype, "myScrollContainer", void 0);
    MessageForm = __decorate([
        core_1.Component({
            selector: 'message-form',
            templateUrl: 'app/message-form/message-form.component.html',
            styleUrls: ['app/message-form/message-form.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], MessageForm);
    return MessageForm;
}());
exports.MessageForm = MessageForm;
//# sourceMappingURL=message-form.component.js.map