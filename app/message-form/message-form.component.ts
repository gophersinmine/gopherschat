import { Component,AfterViewChecked,ElementRef,ViewChild,OnInit } from '@angular/core';

import { messages } from '../shared/message.date';
import { IMessage } from '../shared/message.modal';


@Component({
    selector: 'message-form',
    templateUrl: 'app/message-form/message-form.component.html',
    styleUrls:  ['app/message-form/message-form.component.css']
})
export class MessageForm implements OnInit,AfterViewChecked{
   
    @ViewChild('msgs') private myScrollContainer: ElementRef;
    
    ngOnInit(){
        this.scrollToBottom();
    }

    ngAfterViewChecked() {        
        this.scrollToBottom();        
    } 

    scrollToBottom():void{
        try{
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err){}
    }   

    messages : IMessage[] ;

    constructor(){
        this.messages = messages;
    }

    

}