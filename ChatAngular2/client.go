package main

import (
	"log"

	"github.com/gorilla/websocket"
)

type Client struct {
	message chan []byte
	connect *websocket.Conn
}

func (c *Client) writeMsg() {
	for {
		msg, ok := <-c.message
		if !ok {
			return
		}

		if err := c.connect.WriteMessage(websocket.TextMessage, msg); err != nil {
			log.Println("Disconnected")
			mu.Lock()
			delete(clients, c)
			mu.Unlock()
			c.connect.Close()
		}
	}
}

func (c *Client) readMsg() {
	for {
		_, msg, err := c.connect.ReadMessage()
		if err != nil {
			c.connect.Close()
			log.Println("Disconnected")
			return
		}

		for cl := range clients {
			cl.message <- msg
		}
	}
}
