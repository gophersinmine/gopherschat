package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"math/rand"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

type UsersDB struct {
	id       int64
	name     string
	passHash string
}

func checkErr(err error) {
	if err != nil {
		log.Println(err)
	}
}

func getHash(str string) string {
	hash := md5.New()
	io.WriteString(hash, str)
	return string(hash.Sum(nil))
}

func getSalt(id int64) string {
	salt := ""
	query := fmt.Sprintf("select sals from passalts where id=%d", id)

	rows, err := db.Query(query)
	checkErr(err)

	for rows.Next() {
		err = rows.Scan(&salt)
	}

	return salt
}

func createSalt() string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, 10)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	return string(b)
}

func checkPass(user User) bool {

	udb := UsersDB{}
	id := checkUser(user)

	password := getHash(user.Password)
	salt := getSalt(id)
	guestPass := getHash(password + salt)

	if id != 0 {
		query := fmt.Sprintf("select passwd from users where id=%d", id)

		rows, err := db.Query(query)
		checkErr(err)

		for rows.Next() {
			err = rows.Scan(&udb.passHash)
		}

		if udb.passHash == guestPass {
			return true
		}
	}

	return false
}

func checkUser(user User) int64 {
	udb := UsersDB{}

	query := fmt.Sprintf("select id from users where username='%s'", user.Name)

	rows, err := db.Query(query)
	checkErr(err)

	for rows.Next() {
		err = rows.Scan(&udb.id)
	}

	return udb.id
}

func regUser(user User) bool {

	id := checkUser(user)

	if id == 0 {
		stmt, err := db.Prepare("insert into users(username,passwd) values(?,?)")
		checkErr(err)

		password := getHash(user.Password)
		salt := createSalt()
		passhash := getHash(password + salt)

		res, err := stmt.Exec(user.Name, passhash)
		checkErr(err)

		lastId, err := res.LastInsertId()
		checkErr(err)

		stmt, err = db.Prepare("insert into passalts(id,sals) values(?,?)")
		checkErr(err)

		_, err = stmt.Exec(lastId, salt)
		checkErr(err)

		return true
	}

	return false
}
