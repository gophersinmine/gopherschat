package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	_ "github.com/mattn/go-sqlite3"
)

var (
	clients map[*Client]bool
	mu      sync.Mutex
	db      *sql.DB
)

type User struct {
	Name     string `json:"username"`
	Password string `json:"password"`
}

func RegHandler(w http.ResponseWriter, r *http.Request) {
	user := User{}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	json.Unmarshal(body, &user)

	ans := regUser(user)

	if ans {
		fmt.Fprintf(w, user.Name)
	} else {
		fmt.Fprintf(w, "")
	}
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	user := User{}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	json.Unmarshal(body, &user)

	ans := checkPass(user)

	if ans {
		fmt.Fprintf(w, user.Name)
	} else {
		fmt.Fprintf(w, "")
	}
}

func wsServe(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(w, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		log.Println("Upgrade socket: ", err)
		return
	}

	client := &Client{message: make(chan []byte), connect: conn}

	mu.Lock()
	log.Println("Connected")
	clients[client] = true
	mu.Unlock()

	go client.writeMsg()
	client.readMsg()
}

func main() {
	var err error

	db, err = sql.Open("sqlite3", "users.db")
	if err != nil {
		log.Fatalln("Open database: ", err)
	}
	defer db.Close()

	clients = make(map[*Client]bool)
	fs := http.Dir("/home/blackgopher/VSCode/gopherschat")
	fileHandler := http.FileServer(fs)

	http.Handle("/", fileHandler)
	http.HandleFunc("/chat", wsServe)
	http.HandleFunc("/login", LoginHandler)
	http.HandleFunc("/registration", RegHandler)

	log.Println("Server is running on localhost:3000")
	err = http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatalln("Server is down.")
	}
}
